﻿# SwipeActionAdapter

## 目录
* [功能介绍](#功能介绍)
* [演示](#演示)
* [集成](#集成)
* [使用示例](#使用示例)
* [接口说明](#接口说明)


## 功能介绍

- [x] 适用于ohos的类似邮箱的滑动手势库

## 演示

|设置滑动背景颜色 | 设置可以右滑，不能左滑|
|:---:|:---:|
|<img src="https://gitee.com/openharmony-tpc/SwipeActionAdapter/raw/master/screenshot/color.gif" width="75%"/>|<img src="https://gitee.com/openharmony-tpc/SwipeActionAdapter/raw/master/screenshot/right.gif" width="75%"/>|

|控件左右滑动时会上下滑动，滑动过程中页面会突然复原 |滑动后消失的动画|
|:---:|:---:|
|问题|暂不支持|

## 集成

```
方式一：
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）
添加SwipeActionAdapter.har到entry下的libs文件夹内
entry的build.gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:SwipeActionAdapter:1.0.0'

```


## 使用示例


```
       ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
        listItemProvider = new ContactAdapter(this, contacts,ResourceTable.Layout_row_bg,listContainer,this);
        listContainer.setItemProvider(listItemProvider);

        listItemProvider.addBackground(SwipeDirection.DIRECTION_FAR_LEFT,new RgbColor(255, 0, 255))
                .addBackground(DIRECTION_NORMAL_LEFT, new RgbColor(255, 255, 0))
                .addBackground(DIRECTION_FAR_RIGHT, new RgbColor(0, 255, 255))
                .addBackground(DIRECTION_NORMAL_RIGHT,new RgbColor(255, 0, 0));
```

## 接口说明


```java

   /**
     *  添加背景
     *
     * @param key type
     * @param color 颜色
     * @return ContactAdapter
     */
    public ContactAdapter addBackground(SwipeDirection key, RgbColor color)

    // 该方法主要判断滑动方向：左滑还是右滑。
    @Override
    public boolean hasActions(int position, SwipeDirection direction)

    // 该方法主要判断list item滑动后是否有消失的动画。
    @Override
    public boolean shouldDismiss(int position, SwipeDirection direction)

    // 主要在该方法中处理滑动逻辑
    @Override
    public void onSwipe(int position, SwipeDirection direction)

```
## License

 Copyright (c) 2014 Wouter Dullaert

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
