/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.swipeactionexample;

import com.wdullaer.swipeactionadapter.Contact;
import com.wdullaer.swipeactionadapter.ContactAdapter;
import com.wdullaer.swipeactionadapter.SwipeDirection;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;

import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

import static com.wdullaer.swipeactionadapter.SwipeDirection.DIRECTION_FAR_RIGHT;
import static com.wdullaer.swipeactionadapter.SwipeDirection.DIRECTION_NORMAL_LEFT;
import static com.wdullaer.swipeactionadapter.SwipeDirection.DIRECTION_NORMAL_RIGHT;

/** 主页面 */
public class MainAbility extends Ability implements ContactAdapter.ContactCallbacks{
    private ArrayList<Contact> contacts = new ArrayList<>();
    private ContactAdapter listItemProvider;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initData();
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
        listItemProvider = new ContactAdapter(this, contacts,ResourceTable.Layout_row_bg,listContainer,this);
        listContainer.setItemProvider(listItemProvider);

        listItemProvider.addBackground(SwipeDirection.DIRECTION_FAR_LEFT,new RgbColor(255, 0, 255))
                .addBackground(DIRECTION_NORMAL_LEFT, new RgbColor(255, 255, 0))
                .addBackground(DIRECTION_FAR_RIGHT, new RgbColor(0, 255, 255))
                .addBackground(DIRECTION_NORMAL_RIGHT,new RgbColor(255, 0, 0));
    }

    private void initData() {
        contacts.addAll(Contact.getEnglishContacts());
    }

    // 该方法主要判断滑动方向：左滑还是右滑。
    @Override
    public boolean hasActions(int position, SwipeDirection direction){
        if(direction.isLeft()){
            return false;
        }
        if(direction.isRight()) {
            return true;
        }
        return false;
    }

    // 该方法主要判断list item滑动后是否有消失的动画。
    @Override
    public boolean shouldDismiss(int position, SwipeDirection direction){
        return direction == DIRECTION_NORMAL_LEFT;
    }

    // 主要在该方法中处理滑动逻辑
    @Override
    public void onSwipe(int position, SwipeDirection direction){
        String dir = "";
        switch (direction) {
            case DIRECTION_FAR_LEFT:
                dir = "Far left";
                break;
            case DIRECTION_NORMAL_LEFT:
                dir = "Left";
                break;
            case DIRECTION_FAR_RIGHT:
                dir = "Far right";
                break;
            case DIRECTION_NORMAL_RIGHT:
                dir = "Right";
                break;
        }
        ToastDialog builder = new ToastDialog(this);
        builder.setText(dir ).show();
        listItemProvider.notifyDataChanged();
    }
}
