/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.swipeactionadapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hcs on 2020/12/31.
 */
public class Contact {
    private String index;

    public Contact(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

    /**
     *  创建列表数据
     *
     * @return List<Contact>
     */
    public static List<Contact> getEnglishContacts() {
        List<Contact> contacts = new ArrayList<>();
        for (int i=0;i<20;i++) {
            contacts.add(new Contact("Row "+(i+1)));
        }
        return contacts;
    }
}
